resource "aws_key_pair" "practicalkey" {
  key_name   = "practicalkey"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCZ25JAF6GHJQAp8XAvpgo6xZu25Ruw9E+ucfFDmTqdHvQ6ZVoC6XqkysCI0nKROeMPe9HbwEDcyMCkKJ4m9l4gcGqQb0fX7AMdcS003ii+yiUumOU18GOg4hgLtLHHgGJzUDR0hy0r04yRtZxH8mzFJjaJrTx+mOAUG3akobxoXjWtyCFJF/LdRsmf9At5TFtxwZQABynVzdrOpjbQ72SoCd91H+Sl59vMgkuz3qj3qZimsIlOWyOWE5xe6Tg8CZSSSEBgLZpeL5ALb8iNRuqTW7/Rr1YCAHTwKCO+WtUfiX+JZhVtn2OdMCPDSShCGfqr6/rK/+X9I6PbHxTgUJVxSOeo3czeGcvzvkeILpCEuL1mEvDtlTu6LrNFsRDB48NoAi9T8y01t03iNpTsymc+H+2VoQmBEYY3TuFySbYWRrryAKPgauC5oYBXIBGdVPxpYB13TK/XaMM6hH9E27gKVcqsDVRk/O5y8WD8xdaaeixRJWMX7BfSh4s5oMT3iqk= root@kali"
}
resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.mainvpc.id

  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol  = "tcp"
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "web" {
  ami           = ami-02df9ea15c1778c9c
  instance_type = "t2.micro"

  tags = {
    Name = "HelloWorld"
  }
}


